﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveData : MonoBehaviour {

    public int highScore;



    public void SaveScore()
    {
        Debug.Log("save");
        highScore = GameManager.gM.score;
        Debug.Log(highScore);
        PlayerPrefs.SetInt("Highs Score",highScore);
    }
    public void LoadJson()
    {
        Debug.Log("loaded");
        highScore= PlayerPrefs.GetInt("Highs Score");
        Debug.Log(highScore);
        GameManager.gM.highScore =highScore;
        
    }


}
