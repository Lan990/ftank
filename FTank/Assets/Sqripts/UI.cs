﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour {
    [Header("UI Elements in Game Over and main HUD")]
    public Text lives;//Displays the players lives.
    public Text score;//Displays the plays score.
    public Text highScore;//Displays the top high score.
    public GameObject gameoverScreen;

	// Update is called once per frame
	void Update () {
        lives.text = "Lives: " + GameManager.gM.playerLives.ToString();
        score.text = "Score:" + GameManager.gM.score.ToString();
        highScore.text= "highScore: " + GameManager.gM.highScore.ToString();


    }

    public void gameOver()
    {
        gameoverScreen.SetActive(true);
    }
}
