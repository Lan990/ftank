﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum AIStates { idle, patrol, chase, flee }
public enum AIPersonality { Kamikaze, Basic, QuickShot, Ranged }
public enum AIAvoidanceStates { normal, turnToAvoid, moveToAvoid }

public class AIController3 : MonoBehaviour
{
    public AIStates AIState;
    public AIPersonality AIPersonality;
    [HideInInspector]
    public TankData data;
    [HideInInspector]
    public GameObject player;
	[HideInInspector]
	public GameObject playerTwo;

    // private variables
    //private NoiseMaker noise;
    private Transform tf;

    [Header("Patrol Data")]
    private List<Transform> waypoints = new List<Transform>();
    public int currentWaypoint;
    public float stopDistance;
    public float pauseAtWaypoint = 5.0f;
    public float lastWaypointTime;

    [Header("Sense Data")]
    public bool canSeePlayer = false;
    public float lastStateChangeTime;
    public float sightDistance;
    public float fieldOfView;

    [Header("Flee Data")]
    public float fleeDistance = 5.0f;

    [Header("Avoidance Data")]
    public AIAvoidanceStates avoidState = AIAvoidanceStates.normal;
    public float avoidMovementTime = 1.0f;
    public float lastAvoidanceStateChangeTime;
    public float avoidDistance = 3.0f;
    public float timeToChangeState;

    // Use this for initialization
    void Start()
    {
        GameManager.gM.enemyCount++;
        player = GameManager.gM.player;
		playerTwo = GameManager.gM.secoundPlayer;
        // Get the waypoints
        waypoints.Clear();
        // for each game object with the tag "Waypoint"
        foreach (GameObject waypoint in GameObject.FindGameObjectsWithTag("Way Point"))
        {
            // add waypoints 
            waypoints.Add(waypoint.transform);
        }

        data = GetComponent<TankData>();
        tf = GetComponent<Transform>();

        
        AIState = AIStates.idle;

        lastStateChangeTime = Time.time;
        lastAvoidanceStateChangeTime = Time.time;
        lastWaypointTime = Time.time;

        if (AIPersonality == AIPersonality.Kamikaze)
        {
            stopDistance = 0;
            data.moveSpeed = data.moveSpeed / 2;
        }
        else if (AIPersonality == AIPersonality.Ranged)
        {
            sightDistance = sightDistance * 2;
        }
        else if (AIPersonality == AIPersonality.QuickShot)
        {
            data.fireRate = data.fireRate / 2;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (!player)
        {
            player = GameManager.gM.player;
        }
		if (!playerTwo) {
			playerTwo = GameManager.gM.secoundPlayer;
		}
        // switch case for ai states
        switch (AIState)
        {
            // idle state
            case AIStates.idle:
                doIdle();
                // changes state to chase if the player is seen or heard
                if (playerIsSeen())
                {
                    changeState(AIStates.chase);
                }
                // changes state to patrol if too much time has passed without seeing player
                else if (Time.time - lastStateChangeTime > timeToChangeState)
                {
                    changeState(AIStates.patrol);
                }
                break;
            // patrol state
            case AIStates.patrol:
                doPatrol();
                DoSeekPlayer();
                // if health is below half health, flees from player

                if (data.health <= data.maxHealth / 2)
                {
                    changeState(AIStates.flee);
                }

                // if the time exceeds 10 seconds, goes idle state
                if (Time.time - lastStateChangeTime > 10.0f)
                {
                    changeState(AIStates.idle);
                }
                break;
            // chase state
            case AIStates.chase:
                doChase();
                // if health is below half health, flees from player
                if (data.health <= data.maxHealth / 2)
                    changeState(AIStates.flee);
                break;
            // flee state
            case AIStates.flee:
                DoFleePlayer();
                break;
        }
    }

    void changeState(AIStates newState)
    {
        // time last changed state
        lastStateChangeTime = Time.time;
        // saves the state
        AIState = newState;
    }

    void ChangeAvoidanceState(AIAvoidanceStates newState)
    {
        // sets variable to Time.time
        lastAvoidanceStateChangeTime = Time.time;
        // changes the avoidstate to the new state
        avoidState = newState;
    }

    void DoSeekPlayer()
    {

        switch (avoidState)
        {
            case AIAvoidanceStates.normal:

                // find direction to player
                Vector3 newDirection = player.transform.position - tf.position;

                // rotates to new direction towards player
                SendMessage("RotateTowards", newDirection);

                // moves forward with the forward speed in inspector
                SendMessage("Move", transform.forward * data.moveSpeed);
                break;
            case AIAvoidanceStates.turnToAvoid:
                // rotates 
                SendMessage("Rotate", data.turnSpeed);
                // if raycast hits nothing, change state to movetoavoid
                if (CanMoveForward())
                {
                    ChangeAvoidanceState(AIAvoidanceStates.moveToAvoid);
                }
                break;
            case AIAvoidanceStates.moveToAvoid:
                // move forward
                SendMessage("Move", transform.forward * data.moveSpeed);
                // if raycast hits something, turn to avoid
                if (!CanMoveForward())
                {
                    ChangeAvoidanceState(AIAvoidanceStates.turnToAvoid);
                }
                // if time has passed, move to normal state
                if (Time.time - lastAvoidanceStateChangeTime > avoidMovementTime)
                {
                    ChangeAvoidanceState(AIAvoidanceStates.normal);
                }
                break;
        }
    }

    bool CanMoveForward()
    {
        // Raycast forward
        RaycastHit hitdata;
        // shoots a raycast from position, stores data into hit data
        if (Physics.Raycast(tf.position, tf.forward, out hitdata, avoidDistance))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    void DoFleePlayer()
    {
        goToWaypoint();
    }

    void doIdle()
    {
        // Does nothing
    }

    void doPatrol()
    {
        goToWaypoint();
        // if player is seen or makes noise, chase the player
        if (playerIsSeen())
        {
            AIState = AIStates.chase;
        }
    }

    void doChase()
    {
        // create the variable but set no movement
        Vector3 moveVector = Vector3.zero;

        // sets speed to the tank data in inspector
        float speed = data.moveSpeed;

        // take players position minus the tanks position
        moveVector = player.transform.position - tf.position;
        // rotates to new direction towards player
        SendMessage("RotateTowards", moveVector.normalized * speed, SendMessageOptions.DontRequireReceiver);

        // moves the tank
        SendMessage("Move", moveVector.normalized * speed, SendMessageOptions.DontRequireReceiver);

        if (AIPersonality != AIPersonality.Kamikaze)
        {
            Shoot();
        }


    }

    // list of waypoints for tanks
    void goToWaypoint()
    {
        // create the variable but set no movement
        Vector3 moveVector = Vector3.zero;
        // sets speed to the tank data in inspector
        float speed = data.moveSpeed;

        // takes the time elapsed and minus's the lastwaypointtime
        // if the time is greater than or equal to the time elapsed
        if (Time.time - lastWaypointTime >= pauseAtWaypoint)
        {
            // take players position minus the tanks position
            moveVector = (waypoints[Random.Range(0, waypoints.Count)].transform.position - tf.position);

            // sets the lastwaypointtime to the current time
            lastWaypointTime = Time.time;
        }

        // moves the tank
        SendMessage("Move", moveVector.normalized * speed, SendMessageOptions.DontRequireReceiver);
        // rotates to new direction towards player
        SendMessage("RotateTowards", moveVector.normalized * speed, SendMessageOptions.DontRequireReceiver);

    }


    // checks if player is seen by the enemy
    protected bool playerIsSeen()
    {
        Vector3 vectorToTarget = player.transform.position - tf.position;
        float angleToTarget = Vector3.Angle(tf.forward, vectorToTarget);

        // checks if ray hits player
        RaycastHit mark;

        // checks if enemy has collided with player
        if (angleToTarget <= fieldOfView)
        {
            if (Physics.Raycast(data.transform.position, vectorToTarget, out mark, sightDistance))
            {
				if (mark.collider.gameObject == GameManager.gM.player||mark.collider.gameObject == GameManager.gM.secoundPlayer)
                {
                    return true;
                }
            }
        }
        return false;
    }

    void Shoot()
    {
        if (Time.time > data.nextFire)
        {
            //Debug.Log ("Shooting");
            SendMessage("Fire", SendMessageOptions.DontRequireReceiver);//Calls the rotate function in the mover
            data.nextFire = Time.time + data.fireRate;
        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (AIPersonality == AIPersonality.Kamikaze)
        {
            ParticleSystem particles = GetComponent<ParticleSystem>();

            Debug.Log("Touch");
            if (other.gameObject.GetComponent<PlayerControl>())
            {
                Debug.Log("Boom");

                //Calls the Take Damage function in the take damage script.
                particles.Play();
                other.gameObject.SendMessage("TakeDamage", data.exsplosionDamage, SendMessageOptions.DontRequireReceiver);
                Destroy(gameObject, 0.5f);
            }
        }

    }
    void OnDestroy()
    {
        GameManager.gM.enemyCount--;
    }
}